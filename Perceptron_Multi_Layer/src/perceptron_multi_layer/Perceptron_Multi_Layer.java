
package perceptron_multi_layer;

public class Perceptron_Multi_Layer {

    public static void main(String[] args) {
        MLP mlp = new MLP(2, 2, 1);
        double xIn[] = {0,1} ;
        double y[] = {1};
        
        mlp.treinar(xIn, y, 0.3);
    }
    
}
